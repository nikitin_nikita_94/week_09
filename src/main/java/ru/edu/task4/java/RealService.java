package ru.edu.task4.java;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
@Qualifier("RealService")
public class RealService implements SomeInterface {
    @Override
    public String getName() {
        return "RealService";
    }
}
