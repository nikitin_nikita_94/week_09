package ru.edu.task3.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class MainContainer {

    private DependencyObject dependency;

    @Autowired
    public MainContainer(DependencyObject dependencyBean) {
        dependency = dependencyBean;
    }

    public String getValue() {
        return dependency.getValue();
    }
}
