package ru.edu.task1.java;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class ComponentB {

    private String string;

    public ComponentB(@Value("stringForComponentB") String string) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
