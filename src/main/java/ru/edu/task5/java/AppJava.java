package ru.edu.task5.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import ru.edu.task5.common.PostProcessor;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@Configuration
public class AppJava {
    public static ApplicationContext run(){
        //Из контекста не исключаем класс с конфигурацией ExternalBeans
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class, ExternalBeans.class, PostProcessor.class);
        return context;
    }
}
