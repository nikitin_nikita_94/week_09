package ru.edu.task2.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
@Component
public class Child {

    private TimeKeeper timeKeeper;

    public TimeKeeper getTimeKeeper() {
        return timeKeeper;
    }
}
