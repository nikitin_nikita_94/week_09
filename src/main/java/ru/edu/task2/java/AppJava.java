package ru.edu.task2.java;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Класс для настройки контекста контейнера зависимостей.
 */
@ComponentScan
@Configuration
public class AppJava {

    public static MainContainer run(){
        ApplicationContext context = new AnnotationConfigApplicationContext(AppJava.class);
        return context.getBean(MainContainer.class);
    }
}
