package ru.edu.task3.xml;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppXMLTest {

    public static final String PROM = "PROM";
    public static final String DEBUG = "DEBUG";

    @Test
    public void run() {
       // ru.edu.task3.xml.AppXML.run();
        assertEquals(PROM, AppXML.run(PROM).getValue());
        assertEquals(DEBUG, AppXML.run(DEBUG).getValue());
    }
}